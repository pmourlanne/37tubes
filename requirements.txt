blinker==1.4
Flask==1.1.2
Flask-Migrate==2.5.3
Flask-SQLAlchemy==2.1
gunicorn==20.0.4
rollbar==0.12.1
SQLAlchemy==1.3.22
sqlalchemy-migrate==0.13.0
