import os

basedir = os.path.abspath(os.path.dirname(__file__))


SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(basedir, "app.db")
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, "migrations")
SQLALCHEMY_TRACK_MODIFICATIONS = False


SOUND_FILES_FOLDER = "sound_files/"


try:
    from local_settings import *
except ImportError:
    pass
