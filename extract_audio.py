import json
import os
import subprocess
import sys

SOUND_FILES_FOLDER = "sound_files"
FULL_AUDIO_FILENAME = "full_audio.mp3"
TIMESTAMPS_FILENAME = "timestamps.json"


def get_abs_path(filename):
    dirname, _ = os.path.split(os.path.abspath(__file__))
    return os.path.join(
        dirname,
        SOUND_FILES_FOLDER,
        filename,
    )


def main(start, duration, output_name):
    output_name = output_name.decode("utf-8")

    cmd = u"/usr/bin/ffmpeg -ss {} -t {} -i {} -codec:a libmp3lame -b:a 256k {}.mp3".format(
        start,
        duration,
        get_abs_path(FULL_AUDIO_FILENAME),
        get_abs_path(output_name),
    )

    subprocess.check_call(cmd.encode("utf-8"), shell=True)

    with open(get_abs_path(TIMESTAMPS_FILENAME), "r") as f:
        timestamps = json.load(f)

    timestamps[output_name] = (start, duration)

    with open(get_abs_path(TIMESTAMPS_FILENAME), "w") as f:
        json.dump(timestamps, f)

    subprocess.check_call(
        u"firefox {}.mp3".format(get_abs_path(output_name)), shell=True
    )


if __name__ == "__main__":
    main(*sys.argv[1:])
