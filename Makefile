isort:
	pre-commit run isort --all-files

black:
	pre-commit run black --all-files

lint:
	pre-commit run --all-files

check: lint

serve:
	FLASK_APP=tubes flask run --reload
