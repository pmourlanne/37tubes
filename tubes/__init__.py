import json
import os

import click
from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app():
    app = Flask(__name__)

    app.config.from_object("settings")

    db.init_app(app)
    Migrate(app, db)

    from .views import tubes as tubes_blueprint

    app.register_blueprint(tubes_blueprint)

    return app


app = create_app()


@app.cli.command("import_quotes")
@click.argument("folder")
def import_quotes(folder):
    """Import sound quotes from a folder using folder/quotes.json"""
    from .models import SoundQuote

    folder_path = os.path.join("tubes", app.config["SOUND_FILES_FOLDER"])
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    folder = folder or "sound_files"
    with open(os.path.join(folder, "quotes.json")) as f:
        mapping = json.load(f)

    for tag, quote in mapping.items():
        f = os.path.join(folder, u"{}.mp3".format(tag))
        SoundQuote.create_from_file(f, quote, commit=False)

    db.session.commit()
