import random

from flask import Blueprint, redirect, render_template, send_file, url_for
from sqlalchemy.orm import exc
from werkzeug.exceptions import abort

from tubes.models import SoundQuote

tubes = Blueprint("tubes", __name__)


def render_tubes_template(template_name, **kwargs):
    if "sound_quotes" not in kwargs:
        kwargs["sound_quotes"] = SoundQuote.query.order_by(SoundQuote.order).all()

    return render_template(template_name, **kwargs)


@tubes.route("/")
def home():
    return render_tubes_template("home.html")


@tubes.route("/tag/<tag>")
def sound_quote(tag):
    try:
        sound_quote = SoundQuote.query.filter(SoundQuote.tag == tag).one()
    except exc.NoResultFound:
        abort(404)

    return render_tubes_template("sound_quote.html", sound_quote=sound_quote)


@tubes.route("/sound/<tag>")
def sound_file(tag):
    try:
        sound_quote = SoundQuote.query.filter(SoundQuote.tag == tag).one()
    except exc.NoResultFound:
        abort(404)

    return send_file(sound_quote.get_path(), mimetype="audio/mpeg")


@tubes.route("/tag/random")
def random_sound_quote():
    nb_sound_quotes = SoundQuote.query.count()
    sound_quote = SoundQuote.query.all()[random.randint(0, nb_sound_quotes - 1)]
    return redirect(url_for("tubes.sound_quote", tag=sound_quote.tag))
