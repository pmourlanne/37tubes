import hashlib
import os
from shutil import copyfile

from flask import url_for

from tubes import db

COLORS = [
    "#b80f2b",
    "#33ae8a",
    "#9a8e00",
    "#b1f01f",
    "#1abffa",
    "#a1958c",
    "#8b95db",
    "#d7757e",
    "#ae2862",
    "#f732e7",
    "#a76fa8",
    "#a753f6",
    "#a8ea28",
    "#e49c6d",
    "#f2c436",
    "#2b5764",
]


class SoundQuote(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    tag = db.Column(db.String(64), unique=True)
    quote = db.Column(db.String(512))
    sound_file = db.Column(db.String(128))
    order = db.Column(db.Integer)

    @classmethod
    def create_from_file(cls, file, quote, commit=True):
        from tubes import app

        # We copy the file
        _, filename = os.path.split(file)
        path = os.path.join("tubes", app.config["SOUND_FILES_FOLDER"], filename)
        copyfile(file, path)

        # We create the object in db
        name, _ = os.path.splitext(filename)
        md5 = hashlib.md5(filename.encode("utf-8"))
        order = int(md5.hexdigest(), 16)
        order = int(str(order)[:8])
        soundquote = cls(tag=name, sound_file=path, quote=quote, order=order)

        db.session.add(soundquote)
        if commit:
            db.session.commit()

        return soundquote

    def get_path(self):
        from tubes import app

        return os.path.join(app.config["SOUND_FILES_FOLDER"], "{}.mp3".format(self.tag))

    def get_html_color(self):
        return COLORS[self.order % len(COLORS)]

    def get_absolute_url(self):
        return url_for("tubes.sound_quote", tag=self.tag)

    def get_html_quote(self):
        return "".join(
            [
                u'<div class="citation">{}</div>'.format(q)
                for q in self.quote.split("\n")
            ]
        )
