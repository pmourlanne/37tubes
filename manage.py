import json
import os

from flask.ext.script import Manager

from tubes import app, db
from tubes.models import *

manager = Manager(app)


@manager.command
def syncdb():
    """Create the database tables"""
    print("Using database %s" % db.engine.url)
    db.create_all()
    print("Created tables")


@manager.option("-f", "--folder", help="Folder to import sound files from")
def import_quotes(folder):
    """Import sound quotes from a folder using folder/quotes.json"""
    from tubes.models import SoundQuote

    syncdb()

    folder_path = os.path.join("tubes", app.config["SOUND_FILES_FOLDER"])
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    folder = folder or "sound_files"
    with open(os.path.join(folder, "quotes.json")) as f:
        mapping = json.load(f)

    for tag, quote in mapping.items():
        f = os.path.join(folder, u"{}.mp3".format(tag))
        SoundQuote.create_from_file(f, quote, commit=False)
        print(u"Successfully imported {}".format(tag))

    db.session.commit()


if __name__ == "__main__":
    manager.run()
